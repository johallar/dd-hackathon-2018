window.onSpotifyWebPlaybackSDKReady = () => {
  const token = 'BQB40yjEvOhsbgKMHoq7Tc0_cxrg-9UlUVpQ1liWRMYWCzxs4fhmUW8JLloaS7-ieUjorEGypBF4L58j8rS_ccQylMcNj5GyqfGlv9G2jVAGXE_XykQUGEwYuPZDoHJ5qOJPs0cKL_QimQGsdHfU3XxoSWI6KhUut07XOA';
  const player = window.player = new Spotify.Player({
    name: 'Web Playback SDK Quick Start Player',
    getOAuthToken: cb => { cb(token); }
  });

  // Error handling
  player.addListener('initialization_error', ({ message }) => { console.error(message); });
  player.addListener('authentication_error', ({ message }) => { console.error(message); });
  player.addListener('account_error', ({ message }) => { console.error(message); });
  player.addListener('playback_error', ({ message }) => { console.error(message); });

  // Playback status updates
  player.addListener('player_state_changed', state => { console.log(state); });

  // Ready
  player.addListener('ready', ({ device_id }) => {

    let seed = "spotify:track:7xGfFoTpQ2E7fRF5lN10tr";
    const play = ({
      spotify_uri,
      playerInstance: {
        _options: {
          getOAuthToken,
          id
        }
      }
    }) => {
      getOAuthToken(access_token => {
        window.spotify_token = access_token;
        fetch(`https://api.spotify.com/v1/me/player/play?device_id=${device_id}`, {
          method: 'PUT',
          body: JSON.stringify({ uris: [spotify_uri] }),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
          },
        });
      });
    };

    play({
      playerInstance: player,
      spotify_uri: seed,
    });

    setInterval(() => {
      if (window.currentSongUri != seed) {
        seed = window.currentSongUri;
        play({
          playerInstance: player,
          spotify_uri: window.currentSongUri
        });
      }
    }, 1000)

  });

  // Not Ready
  player.addListener('not_ready', ({ device_id }) => {
    console.log('Device ID has gone offline', device_id);
  });

  // Connect to the player!
  player.connect();
};
