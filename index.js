

const spotifyToken = 'BQAMdJajwRddtze6HQQYG1F7qeFzpvKBDrB_QJbkTBsskQbWlSVuINXdLg_hP0tkv1c0KourYbZQcYDUrK9w5iuL__J84VIHFaJyYO-rj4CfQFhTYiC8o-v17IW3wB7FhY3fMOyXfYodo1TtRZd3s7G3t-EHGbIC8rh6Xw';
const spotifyApi = new SpotifyWebApi();
spotifyApi.setAccessToken(spotifyToken);


const ui = SongUI({
    el: document.querySelector('.songui'),
    recommend: function (request) {
        // hit recommendation service for next song...

        spotifyApi.getRecommendations(request).then((data) => {
            console.log(data);
            loadTrack(data.tracks[1]);
        });
    }
});


function loadTrack(track) {
    console.log(track);
    spotifyApi.getAudioFeaturesForTrack(track.id).then((features) => {
        console.log(features);
        ui.setSong(track, features);
    });

}

function loadSong(trackId) {
    spotifyApi.getTracks([trackId]).then((data) => {
        let song = data.tracks[0];
        loadTrack(song);
    });
};

const seedTrack = '6olUplztLFFfU7fMYmFXOP';
loadSong(seedTrack);