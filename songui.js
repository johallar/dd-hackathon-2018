


// UI to display song details.
const SongUI = function (options) {
    var _this = {},
        _initialize;


    // create ui
    _initialize = function () {
        _this.el = options.el || document.createElement('div');

        _this.el.classList.add('songui');

        _this.el.innerHTML = `
                <div>
                  <h1>Turnt Machine</h1>
                  <button class="next">Next</button>
                </div>
                <section class="current-song shake-hard">
                <header>
                    <h1 class="title">Title</h1>
                    <h2 class="artist">Artist</h2>
                </header>

                <img class="image" alt="Album artwork"/>
                </section>

                <aside class="controls">
                    <h2>Features</h2>

                    <label for="control-danceability">Danceability</label>
                    <input id="control-danceability" type="range" min="0" max="1" step="any"/>

                    <label for="control-energy">Energy</label>
                    <input id="control-energy" type="range" min="0" max="1" step="any"/>

                    <label for="control-speechiness">Speechiness</label>
                    <input id="control-speechiness" type="range" min="0" max="1" step="any"/>

                    <label for="control-acousticness">Acousticness</label>
                    <input id="control-acousticness" type="range" min="0" max="1" step="any"/>

                    <label for="control-instrumentalness">Instrumentalness</label>
                    <input id="control-instrumentalness" type="range" min="0" max="1" step="any"/>

                    <label for="control-liveness">Liveness</label>
                    <input id="control-liveness" type="range" min="0" max="1" step="any"/>
                    <br/>
                </aside>
            </section>
        `;
        _this.title = _this.el.querySelector('.title');
        _this.artist = _this.el.querySelector('.artist');
        _this.image = _this.el.querySelector('.image');

        _this.controlEl = _this.el.querySelector('.controls');
        _this.controls = {
            'danceability': _this.el.querySelector('#control-danceability'),
            'energy': _this.el.querySelector('#control-energy'),
            'speechiness': _this.el.querySelector('#control-speechiness'),
            'acousticness': _this.el.querySelector('#control-acousticness'),
            'instrumentalness': _this.el.querySelector('#control-instrumentalness'),
            'liveness': _this.el.querySelector('#control-liveness')
        };
        _this.recommend = _this.el.querySelector('.next');
        _this.recommend.addEventListener('click', _this.onRecommend);
    };

    // when user clicks recommend button
    _this.onRecommend = function (e) {
        if (typeof options.recommend !== 'function') {
            return;
        }
        var query = {};
        if (_this.song) {
            query.seed_tracks = [_this.song.id];
        }
        Object.keys(_this.controls).forEach((key) => {
            query['target_' + key] = _this.controls[key].value;
        });
        options.recommend(query);
    };

    // update the song being shown
    _this.setSong = function (song, features) {
        _this.song = song;
        _this.song_features = features;
        _this.render();
    };

    // update song that is shown
    _this.render = function () {
        if (!_this.song) {
            _this.el.classList.add('loading');
            return;
        }

        window.currentSongUri = _this.song.uri;
        _this.title.innerHTML = _this.song.name;
        _this.artist.innerHTML = _this.song.artists[0].name;
        _this.image.src = _this.song.album.images[1].url;
        _this.el.classList.remove('loading');

        if (!_this.song_features) {
            _this.controlEl.classList.add('loading');
            return
        }

        var features = _this.song_features;
        _this.controls['danceability'].value = features.danceability;
        _this.controls['energy'].value = features.energy;
        _this.controls['speechiness'].value = features.speechiness;
        _this.controls['acousticness'].value = features.acousticness;
        _this.controls['instrumentalness'].value = features.instrumentalness;
        _this.controls['liveness'].value = features.liveness;
        _this.controlEl.classList.remove('loading');
    };


    _initialize();
    // loading
    _this.setSong(null);
    return _this;
};
